package com.hcl.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.hcl.model.BankTransactions;

@Service
public class PaymentService {

	public BankTransactions getAllTransactions() {
		RestTemplate restTemplate = new RestTemplate();

		BankTransactions bt = restTemplate.getForObject(
				"https://apisandbox.openbankproject.com/obp/v1.2.1/banks/rbs/accounts/savings-kids-john/public/transactions",
				BankTransactions.class);
		System.out.println(bt.toString());
		return bt;
	}
}
