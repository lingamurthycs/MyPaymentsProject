package com.hcl.model;

public class Metadata {
	private String narrative;
	private String[] comments;
	private String[] tags;

	private String[] images;

	private String where;

	public String[] getTags() {
		return tags;
	}

	public void setTags(String[] tags) {
		this.tags = tags;
	}

	public String getNarrative() {
		return narrative;
	}

	public void setNarrative(String narrative) {
		this.narrative = narrative;
	}

	public String[] getImages() {
		return images;
	}

	public void setImages(String[] images) {
		this.images = images;
	}

	public String getWhere() {
		return where;
	}

	public void setWhere(String where) {
		this.where = where;
	}

	public String[] getComments() {
		return comments;
	}

	public void setComments(String[] comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "ClassPojo [tags = " + tags + ", narrative = " + narrative + ", images = " + images + ", where = "
				+ where + ", comments = " + comments + "]";
	}
}
