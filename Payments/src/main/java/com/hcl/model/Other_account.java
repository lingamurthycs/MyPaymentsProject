package com.hcl.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Other_account {
	private String id;

	private Holder holder;

	private String counterpartyAccount;
	private String kind;
	private String IBAN;

	private String swift_bic;
	private Bank bank;
	private OtherAccountMetadata metadata;

	public Bank getBank() {
		return bank;
	}

	public String getCounterpartyAccount() {
		return counterpartyAccount;
	}

	public Holder getHolder() {
		return holder;
	}

	public String getIBAN() {
		return IBAN;
	}

	public String getId() {
		return id;
	}

	public String getKind() {
		return kind;
	}

	public OtherAccountMetadata getMetadata() {
		return metadata;
	}

	public String getSwift_bic() {
		return swift_bic;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public void setCounterpartyAccount(String counterpartyAccount) {
		this.counterpartyAccount = counterpartyAccount;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public void setIBAN(String iBAN) {
		IBAN = iBAN;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonCreator
	public Other_account(@JsonProperty("number") String counterpartyAccount) {
		super();
		this.counterpartyAccount = counterpartyAccount;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public void setMetadata(OtherAccountMetadata metadata) {
		this.metadata = metadata;
	}

	public void setSwift_bic(String swift_bic) {
		this.swift_bic = swift_bic;
	}

	@Override
	public String toString() {
		return "Other_account [id=" + id + ", holder=" + holder + ", swift_bic=" + swift_bic + ", bank=" + bank
				+ ", counterpartyAccount=" + counterpartyAccount + ", IBAN=" + IBAN + ", kind=" + kind + ", metadata="
				+ metadata + "]";
	}

}