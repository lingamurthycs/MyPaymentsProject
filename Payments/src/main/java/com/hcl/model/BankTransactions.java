package com.hcl.model;

public class BankTransactions {
	private Transactions[] transactions;

	public Transactions[] getTransactions() {
		return transactions;
	}

	public void setTransactions(Transactions[] transactions) {
		this.transactions = transactions;
	}

	@Override
	public String toString() {
		return "ClassPojo [transactions = " + transactions + "]";
	}
}
