package com.hcl.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Transactions {
	private String accountId;
	private This_account this_account;
	private Other_account other_account;
	private Details details;
	private Metadata metadata;

	public String getAccountId() {
		return accountId;
	}

	public Details getDetails() {
		return details;
	}

	public Metadata getMetadata() {
		return metadata;
	}

	public Other_account getOther_account() {
		return other_account;
	}

	public This_account getThis_account() {
		return this_account;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	@JsonCreator
	public Transactions(@JsonProperty("id") String accountId) {
		super();
		this.accountId = accountId;
	}

	public void setDetails(Details details) {
		this.details = details;
	}

	public void setMetadata(Metadata metadata) {
		this.metadata = metadata;
	}

	public void setOther_account(Other_account other_account) {
		this.other_account = other_account;
	}

	public void setThis_account(This_account this_account) {
		this.this_account = this_account;
	}

}