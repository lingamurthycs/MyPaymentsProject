package com.hcl.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Holder {
	private String is_alias;

	private String counterpartyName;

	@JsonCreator
	public Holder(@JsonProperty("name") String counterpartyName) {
		super();
		this.counterpartyName = counterpartyName;
	}

	public String getCounterpartyName() {
		return counterpartyName;
	}

	public String getIs_alias() {
		return is_alias;
	}

	public void setCounterpartyName(String counterpartyName) {
		this.counterpartyName = counterpartyName;
	}

	public void setIs_alias(String is_alias) {
		this.is_alias = is_alias;
	}

}