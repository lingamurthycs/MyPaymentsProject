package com.hcl.model;
public class New_balance
{
    private double amount;

    private String currency;

    public double getAmount ()
    {
        return amount;
    }

    public void setAmount (double amount)
    {
        this.amount = amount;
    }

    public String getCurrency ()
    {
        return currency;
    }

    public void setCurrency (String currency)
    {
        this.currency = currency;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [amount = "+amount+", currency = "+currency+"]";
    }
}