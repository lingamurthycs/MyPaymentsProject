package com.hcl.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Details {

	private String transactionType;
	private String description;
	private String posted;
	private String completed;
	private New_balance new_balance;
	private Value value;

	@JsonCreator
	public Details(@JsonProperty("type") String transactionType) {
		super();
		this.transactionType = transactionType;
	}

	public String getCompleted() {
		return completed;
	}

	public String getDescription() {
		return description;
	}

	public New_balance getNew_balance() {
		return new_balance;
	}

	public String getPosted() {
		return posted;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public Value getValue() {
		return value;
	}

	public void setCompleted(String completed) {
		this.completed = completed;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setNew_balance(New_balance new_balance) {
		this.new_balance = new_balance;
	}

	public void setPosted(String posted) {
		this.posted = posted;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public void setValue(Value value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Details [new_balance=" + new_balance + ", description=" + description + ", value=" + value
				+ ", transactionType=" + transactionType + ", posted=" + posted + ", completed=" + completed + "]";
	}

}
