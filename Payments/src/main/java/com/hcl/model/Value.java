package com.hcl.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Value
{
    private String instructedAmount;

    private String instructedCurrency;

    @JsonCreator
	public Value(@JsonProperty("amount") String instructedAmount, @JsonProperty("currency") String instructedCurrency) {
		super();
		this.instructedAmount = instructedAmount;
		this.instructedCurrency = instructedCurrency;
	}

	public String getInstructedAmount() {
		return instructedAmount;
	}

	public void setInstructedAmount(String instructedAmount) {
		this.instructedAmount = instructedAmount;
	}

	public String getInstructedCurrency() {
		return instructedCurrency;
	}

	public void setInstructedCurrency(String instructedCurrency) {
		this.instructedCurrency = instructedCurrency;
	}

	@Override
	public String toString() {
		return "Value [instructedAmount=" + instructedAmount + ", instructedCurrency=" + instructedCurrency + "]";
	}


}
			
		