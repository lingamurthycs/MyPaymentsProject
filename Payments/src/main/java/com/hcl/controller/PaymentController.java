package com.hcl.controller;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.model.BankTransactions;
import com.hcl.model.Transactions;
import com.hcl.service.PaymentService;

@RestController
public class PaymentController {

	@Autowired
	private PaymentService paymentService;

	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BankTransactions listTransactions() {
		BankTransactions bt = paymentService.getAllTransactions();
		return bt;
	}

	@RequestMapping(value = "/list/{type}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BankTransactions listTransactionsByType(@PathVariable("type") String type) {

		BankTransactions bt = listTransactions();

		bt.setTransactions(Arrays.stream(bt.getTransactions())
				.filter(trans -> (trans != null && trans.getDetails() != null
						&& trans.getDetails().getTransactionType() != null
						&& trans.getDetails().getTransactionType().equals(type)))
				.toArray(size -> new Transactions[size]));
		
		return bt;
	}

	@RequestMapping(value = "/list/{type}/amount", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public double amountByTransactionType(@PathVariable("type") String type) {
		BankTransactions bt = listTransactionsByType(type);

		return Arrays.stream(bt.getTransactions())
				.filter(trans -> trans != null && trans.getDetails() != null
						&& trans.getDetails().getTransactionType() != null && trans.getDetails().getValue() != null)
				.mapToDouble(trans -> Double.parseDouble(trans.getDetails().getValue().getInstructedAmount())).sum();

	}

}
